import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

public class Wxhw
{
	public String getWx(String zipCode)
	{
    JsonElement jse = null;
    String wxReport = null;

		try
		{
			// Construct WxStation API URL
			URL wURL = new URL("http://api.wunderground.com/api/8100a7e7f3bdbd74/conditions/q/"
					+ zipCode
					+ ".json");

			// Open the URL
			InputStream is = wURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
        try
        {
          String error = jse.getAsJsonObject().get("response")
                            .getAsJsonObject().get("error")
                            .getAsJsonObject().get("description").getAsString();
          wxReport = "Error: " + error;
        }
        catch (NullPointerException e)
        {
          // Build a weather report
          String location = jse.getAsJsonObject().get("current_observation")
                               .getAsJsonObject().get("display_location")
                               .getAsJsonObject().get("full").getAsString();
          wxReport = "Location:           " + location + "\n";

          String time = jse.getAsJsonObject().get("current_observation")
                           .getAsJsonObject().get("observation_time").getAsString();
          wxReport = wxReport + "Time:               " + time + "\n";

          String weather = jse.getAsJsonObject().get("current_observation")
                              .getAsJsonObject().get("weather").getAsString();
          wxReport = wxReport + "Weather:            " + weather + "\n";

          String temp = jse.getAsJsonObject().get("current_observation")
                           .getAsJsonObject().get("temp_f").getAsString();
          wxReport = wxReport + "Temperature F:      " + temp + "\n";

          String wind = jse.getAsJsonObject().get("current_observation")
                           .getAsJsonObject().get("wind_string").getAsString();
          wxReport = wxReport + "Wind:               " + wind + "\n";
      
          String pressure = jse.getAsJsonObject().get("current_observation")
                               .getAsJsonObject().get("pressure_in").getAsString();
          wxReport = wxReport + "Pressure in HG:     " + pressure;
        }
		}
    return wxReport;
	}

	public static void main(String[] args)
	{
		Wxhw b = new Wxhw();
    if ( args.length == 0 )
      System.out.println("Please enter a Zipcode as the first argument.");
    else
    {
		  String wx = b.getWx(args[0]);
      if ( wx != null )
		    System.out.println(wx);
    }
	}
}
